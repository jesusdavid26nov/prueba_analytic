<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    use HasFactory;

    protected $fillable = [
        'nombre',
        'apellido',
        'email',
        'categoria',
        'telefono',
        'direccion'
    ];
    public function storeCliente($request)
    {
        $cliente = self::create($request->all());
        return $cliente;
    }
    public function updateCliente($request)
    {
        $cliente = self::update($request->all());
        return $cliente ;
    }

}
