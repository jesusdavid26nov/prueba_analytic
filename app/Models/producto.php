<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    use HasFactory;
    protected $fillable = [
        'num_factura',
        'nombre',
        'precio',
        'stock'
    ];
    public function storeProducto($request)
    {
        $producto = self::create($request->all());
        return $producto;
    }
    public function updateProducto($request)
    {
        $producto = self::update($request->all());
        return $producto ;
    }
}