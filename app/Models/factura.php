<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class factura extends Model
{
    use HasFactory;

    protected $fillable = [
        'num_factura',
        'id_cliente',
        'id_producto',
        'fecha'
    ];

    public function storeFactura($request)
    {
        $factura = self::create($request->all());
        return $factura;
    }
    
    public function updateFactura($request)
    {
        $factura = self::update($request->all());
        return $factura ;
    }
}
